# from setuptools import setup
import os, platform, pkg_resources
from distutils.core import setup, Extension
from Cython.Distutils import build_ext
from autowrap.Main import run as autowrap_run

data_dir = pkg_resources.resource_filename("autowrap", "data_files")
include_dir = os.path.join(data_dir, "autowrap")

print('PRECOMPILE USING AUTOWRAP')
autowrap_run([os.path.abspath('./parser/BinFileParser.pxd')], [], [], 
               os.path.abspath('./parser/py_BinFileParser.pyx'), )

ext = Extension("py_BinFileParser",
                sources = ['parser/py_BinFileParser.cpp'],
                language="c++",
                extra_compile_args=["-std=c++11"], #Release mode (no -g switch)
                extra_link_args=["-std=c++11"], #Release mode (no -g switch)
#                 extra_compile_args=["-std=c++11", "-g"], #Debug mode (no -g switch)
#                 extra_link_args=["-std=c++11",  "-g"], #Debug mode (no -g switch)
                include_dirs = [include_dir, data_dir],
               )
setup(
    cmdclass={"build_ext": build_ext},
    name="py_BinFileParser",
    version="0.0.1",
    ext_modules=[ext],
)
