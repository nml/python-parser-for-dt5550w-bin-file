'''
Description: Plot spectra
Author: Ming Fang
Date: 2022-09-20 21:06:52
LastEditors: Ming Fang
LastEditTime: 2022-09-20 22:53:00
'''
import tkinter
from tkinter import ttk
from matplotlib.backends.backend_tkagg import (
    FigureCanvasTkAgg, NavigationToolbar2Tk)
# Implement the default Matplotlib key bindings.
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import config


def plotSpectra(root):
    def SiPMID_changed(event):
        AsicID = int(selectedAsicID.get())
        channelID = int(selectedChannelID.get())
        SiPMID = config.imagerSetup.getChannelID((AsicID, channelID))
        # print(AsicID, channelID, SiPMID)
        # update datas
        line1.set_data(config.singleSiPMSpectra[SiPMID].binCenters, config.singleSiPMSpectra[SiPMID].binCounts)
        ax1.set_title('ASIC {0:d}, Channel {1:d}. Total counts {2:d}'.format(AsicID, channelID, config.singleSiPMSpectra[SiPMID].getTotalCounts()))
        # rescale
        # recompute the ax.dataLim
        ax1.relim()
        # update ax.viewLim using the new dataLim
        ax1.autoscale_view()
        # required to update canvas and attached toolbar!
        canvas.draw()
    
    def crystalID_changed(event):
        crystalID = int(selectedCrystalID.get())
        # print(crystalID)    
        # update datas
        line2.set_data(config.SiPMPairSpectra[crystalID].binCenters, config.SiPMPairSpectra[crystalID].binCounts)
        ax2.set_title('Crystal {0:d}. Total counts {1:d}'.format(crystalID, config.SiPMPairSpectra[crystalID].getTotalCounts()))
        # rescale
        # recompute the ax.dataLim
        ax2.relim()
        # update ax.viewLim using the new dataLim
        ax2.autoscale_view()
        # required to update canvas and attached toolbar!
        canvas.draw()

    topFrame = tkinter.Frame(root)
    bottomFrame = tkinter.Frame(root)

    # create AsicID combobox
    AsicIDLabel = ttk.Label(text="ASIC: ")
    selectedAsicID = tkinter.StringVar()
    AsicID_combobox = ttk.Combobox(root, textvariable=selectedAsicID)
    AsicID_combobox['values'] = ['0', '1', '2', '3']
    AsicID_combobox['state'] = 'readonly'
    AsicID_combobox.current(0)
    AsicID_combobox.bind('<<ComboboxSelected>>', SiPMID_changed)

    # create ChannelID combobox
    channelIDLabel = ttk.Label(text="Channel: ")
    selectedChannelID = tkinter.StringVar()
    channelID_combobox = ttk.Combobox(root, textvariable=selectedChannelID)
    channelID_combobox['values'] = [str(i) for i in range(2, 16)]
    channelID_combobox['state'] = 'readonly'
    channelID_combobox.current(0)
    channelID_combobox.bind('<<ComboboxSelected>>', SiPMID_changed)
    
    # create crystalID combobox
    crystalIDLabel = ttk.Label(text="Crystal: ")
    selectedCrystalID = tkinter.StringVar()
    crystalID_combobox = ttk.Combobox(root, textvariable=selectedCrystalID)
    crystalID_combobox['values'] = [str(i) for i in range(28)]
    crystalID_combobox['state'] = 'readonly'
    crystalID_combobox.current(0)
    crystalID_combobox.bind('<<ComboboxSelected>>', crystalID_changed)

    fig = Figure(figsize=(8, 8), dpi=100)
    ax1 = fig.add_subplot(221)
    AsicID = int(selectedAsicID.get())
    channelID = int(selectedChannelID.get())
    SiPMID = config.imagerSetup.getChannelID((AsicID, channelID))
    # print(SiPMID)
    line1, = ax1.step(config.singleSiPMSpectra[SiPMID].binCenters, config.singleSiPMSpectra[SiPMID].binCounts, where='mid')
    ax1.set_title('ASIC {0:d}, Channel {1:d}. Total counts {2:d}'.format(AsicID, channelID, config.singleSiPMSpectra[SiPMID].getTotalCounts()))
    ax1.set_xlabel("Energy (keV)")
    ax1.set_ylabel("Counts")

    ax2 = fig.add_subplot(222)
    crystalID = int(selectedAsicID.get())
    # print(crystalID)
    line2, = ax2.step(config.SiPMPairSpectra[crystalID].binCenters, config.SiPMPairSpectra[crystalID].binCounts, where='mid')
    ax2.set_title('Crystal {0:d}. Total counts {1:d}'.format(crystalID, config.SiPMPairSpectra[crystalID].getTotalCounts()))
    ax2.set_xlabel("Energy (keV)")
    ax2.set_ylabel("Counts")

    ax3 = fig.add_subplot(223)
    line3, = ax3.step(config.coincidenceSpectra.binCenters, config.coincidenceSpectra.binCounts, where='mid')
    ax3.set_title('Coincidence spectrum. Total counts {0:d}'.format(config.coincidenceSpectra.getTotalCounts()))
    ax3.set_xlabel("Energy (keV)")
    ax3.set_ylabel("Counts")

    canvas = FigureCanvasTkAgg(fig, master=root)  # A tk.DrawingArea.
    canvas.draw()

    # pack_toolbar=False will make it easier to use a layout manager later on.
    toolbar = NavigationToolbar2Tk(canvas, root, pack_toolbar=False)
    toolbar.update()

    canvas.mpl_connect(
        "key_press_event", lambda event: print(f"you pressed {event.key}"))
    canvas.mpl_connect("key_press_event", key_press_handler)

    # Packing order is important. Widgets are processed sequentially and if there
    # is no space left, because the window is too small, they are not displayed.
    # The canvas is rather flexible in its size, so we pack it last which makes
    # sure the UI controls are displayed as long as possible.
    AsicIDLabel.pack(in_=topFrame, side=tkinter.LEFT, fill=tkinter.X)
    AsicID_combobox.pack(in_=topFrame, side=tkinter.LEFT, fill=tkinter.X)
    channelIDLabel.pack(in_=topFrame, side=tkinter.LEFT, fill=tkinter.X)
    channelID_combobox.pack(in_=topFrame, side=tkinter.LEFT, fill=tkinter.X)
    crystalIDLabel.pack(in_=topFrame, side=tkinter.LEFT, fill=tkinter.X)
    crystalID_combobox.pack(in_=topFrame, side=tkinter.LEFT, fill=tkinter.X)
    toolbar.pack(in_=bottomFrame, side=tkinter.TOP, fill=tkinter.X)
    canvas.get_tk_widget().pack(in_=bottomFrame, side=tkinter.TOP, fill=tkinter.BOTH, expand=True)
    topFrame.pack(side=tkinter.TOP)
    bottomFrame.pack(side=tkinter.BOTTOM, fill=tkinter.BOTH, expand=True)
