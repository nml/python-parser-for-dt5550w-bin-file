'''
Description: Select coincidences between two SiPM channels on the same crystal, and coincidences between two crystals.
Author: Ming Fang
Date: 2022-09-19 17:50:22
LastEditors: Ming Fang mingf2@illinois.edu
LastEditTime: 2022-09-20 22:21:22
'''
from sources.utilities import Histogram, ImagerSetup, SiPMEvent, CrystalEvent, CoincidenceEvent
from sources.py_BinFileParser import CITIROCEvent
import numpy as np
from typing import List
import config


def processCITIROCEventChunk(chunk: List[CITIROCEvent]):
    """Process a chunk of events from binary file.

    Args:
        chunk (List[CITIROCEvent]): A list of CITIROC events.
    """
    packets2Process = []
    lastTimeStamp = 0
    for newEvent in chunk:
        currentTimeStamp = newEvent.RunEventTimecode * 0.5  # ns
        if len(packets2Process) > 0 and abs(currentTimeStamp -
                                            lastTimeStamp) >= config.imagerSetup.timeWindow:
            processCoincidentCITIROCEvents(packets2Process)
            packets2Process.clear()
        lastTimeStamp = currentTimeStamp
        packets2Process.append(newEvent)


def processCoincidentCITIROCEvents(packets: List[CITIROCEvent]) -> None:
    """Process the CITIROC events that are in coincidence. Update the spectra and the list of coincidence events.

    Args:
        packets (List[CITIROCEvent]): A list of CITIROC events.
    """
    SiPMEvents = []
    if extractSiPMEvents(packets, SiPMEvents):
        SiPMEvents.sort(key=lambda x: x.SiPMID)
        crystalEvents = []
        if extractCrystralEvents(SiPMEvents, crystalEvents):
            crystalEvents.sort(key=lambda x: x.eventTimeStamp)
            extractCoincidenceEvents(crystalEvents)


def extractSiPMEvents(packets: List[CITIROCEvent], SiPMEvents: List[SiPMEvent]) -> bool:
    """Extract the SiPM events with detected light above threshold. Update the spectra in each SiPM channel.

    Args:
        packets (List[CITIROCEvent]): A list of CITIROC events.
        SiPMEvents (List[SiPMEvent]): A list of SiPM events. Calibrated.

    Returns:
        bool: True if the number of SiPM events returned is non-zero.
    """
    for p in packets:
        for i in range(32):
            key = (p.AsicID, i)
            if not config.imagerSetup.isChannelEnabled(key):
                continue
            sipmEvent = SiPMEvent()
            calicoef = config.imagerSetup.getChannelCalibrationCoef(key)
            # energy calibration
            sipmEvent.lightDetected = calicoef[0] * p.chargeLG[i] + calicoef[1]
            if sipmEvent.lightDetected < config.imagerSetup.getChannelEnergyThreshold(key):
                continue
            sipmEvent.AsicID = p.AsicID
            sipmEvent.channelID = i
            sipmEvent.SiPMID = config.imagerSetup.getChannelID(key)
            sipmEvent.eventTimeStamp = 0.5 * p.RunEventTimecode
            SiPMEvents.append(sipmEvent)
            # update spectra of each SiPM channel
            config.singleSiPMSpectra[config.imagerSetup.getChannelID(key)].fill(sipmEvent.lightDetected)
    return len(SiPMEvents) > 0


def extractCrystralEvents(SiPMEvents: List[SiPMEvent],
                          crystalEvents: List[CrystalEvent]) -> bool:
    """Extract the energy deposition events in a crystal by finding coincidence between top and bottom SiPMs. Update the spectra of deposited in each crystal.

    Args:
        SiPMEvents (List[SiPMEvent]): A list of SiPM events.
        crystalEvents (List[CrystalEvent]): A list of energy deposition events in each crystal.

    Returns:
        bool: True if the number of events returned is non-zero.
    """
    for i in range(len(SiPMEvents)):
        # use the fact that the ID of top and bottom SiPM are neighbouring
        if SiPMEvents[i].SiPMID % 2 == 1:
            continue
        j = i + 1
        if j >= len(SiPMEvents):
            break
        if SiPMEvents[j].SiPMID - SiPMEvents[i].SiPMID != 1:
            continue
        # current and next event is a pair
        crystalEvent = CrystalEvent()
        crystalEvent.crystalID = SiPMEvents[i].SiPMID // 2
        crystalEvent.energyDeposited = SiPMEvents[i].lightDetected + \
            SiPMEvents[j].lightDetected
        crystalEvent.eventTimeStamp = (SiPMEvents[i].eventTimeStamp +
                                       SiPMEvents[j].eventTimeStamp) / 2
        crystalEvent.deltaT = SiPMEvents[i].eventTimeStamp - \
            SiPMEvents[j].eventTimeStamp
        key = (SiPMEvents[i].AsicID, SiPMEvents[i].channelID)
        p1 = config.imagerSetup.getChannelPosition(key)
        key = (SiPMEvents[j].AsicID, SiPMEvents[j].channelID)
        p2 = config.imagerSetup.getChannelPosition(key)
        z = findZPosition(SiPMEvents[i].lightDetected, p1[2],
                          SiPMEvents[j].lightDetected, p2[2])
        crystalEvent.position = np.array([p1[0], p1[1], z])
        crystalEvents.append(crystalEvent)
        # update SiPM Pair spectra
        config.SiPMPairSpectra[crystalEvent.crystalID].fill(crystalEvent.energyDeposited)
    return len(crystalEvents) > 0


def extractCoincidenceEvents(crystalEvents: List[CrystalEvent]) -> bool:
    """Extract the coincidence events between two crystals. Update the coincidence spectrum and list of coincidence.

    Args:
        crystalEvents (List[CrystalEvent]): A list of crystal energy deposition events.

    Returns:
        bool: True if a coincidence is found.
    """
    # expect two events in a series
    if (len(crystalEvents) != 2):
        return False
    coincidence = CoincidenceEvent()
    coincidence.firstEnergyDeposited = crystalEvents[0].energyDeposited
    coincidence.secondEnergyDeposited = crystalEvents[1].energyDeposited
    coincidence.firstInteractionPosition = crystalEvents[0].position
    coincidence.secondInteractionPosition = crystalEvents[1].position
    # update coincidence spectrum
    config.coincidenceSpectra.fill(coincidence.firstEnergyDeposited +
                                   coincidence.secondEnergyDeposited)
    # upadte list of coincidences
    config.coincidences.append(coincidence)
    return True


def findZPosition(e1: float, z1: float, e2: float, z2: float) -> float:
    """Find the z coordinate of interaction position. Assumes a linear relationship. Will be updated after characterizing the detector crystal.

    Args:
        e1 (float): Light detected by the first SiPM, keV
        z1 (float): z coordinate of the first SiPM
        e2 (float): Light detected by the second SiPM, keV
        z2 (float): z coordinate of the second SiPM

    Returns:
        float: z coordainate of the interaction position.
    """
    return (e1 * z1 + e2 * z2) / (e1 + e2)
