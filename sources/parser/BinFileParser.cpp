#include "BinFileParser.h"

std::vector<CITIROCEvent> DT5550WBinFile::createNDummyEvents(int n)
{
    std::vector<CITIROCEvent> events;
    for (int i = 0; i < n; i++)
    {
        // create dummpy event
        CITIROCEvent newEvent;
        newEvent.EventCounter = i;
        newEvent.AsicID = i;
        newEvent.RunEventTimecode = i;
        // newEvent.RunEventTimecode_ns = 0.5 * i;
        for (int j = 0; j < 32; j++)
        {
            newEvent.chargeLG[j]= (j+i);
            newEvent.chargeHG[j] = (j+i);
        }
        newEvent.hit = 0xFFFF;
        events.push_back(newEvent);
    }
    return events;
}

std::vector<CITIROCEvent> DT5550WBinFile::readNextNEvents(int n)
{
    std::vector<CITIROCEvent> events;
    if (n < 1)
    {
        return events;
    }
    
    // parse data
    char* buffer = new char[n*eventSize_];
    int numberOfEvents(0);
    int64_t numberOfBytesLeft(0);
    int64_t bufferSize(n*eventSize_);
    while (numberOfEvents < n && fileObj_.peek() != EOF)
    {
        fileObj_.read(buffer, bufferSize);
        bufferSize = fileObj_.gcount();
        numberOfBytesLeft = bufferSize;
        if (bufferSize < eventSize_)
        {
            break;
        }
        // decode buffer
        decode(buffer, bufferSize, events, numberOfEvents, numberOfBytesLeft);
        // check if we have any bytes left in the buffer
        if (numberOfBytesLeft != 0)
        {
            fileObj_.seekg(-numberOfBytesLeft, std::ios_base::cur);
        }
        if (numberOfEvents < n)
        {
            bufferSize = (n-numberOfEvents)*eventSize_;
            numberOfBytesLeft = bufferSize;
        }
    }
    numberOfEventsRead_ += numberOfEvents;
    delete[] buffer;
    return events;
}

void DT5550WBinFile::decode(const char* buffer, const int64_t bufferSize, 
                            std::vector<CITIROCEvent>& events, 
                            int& numberOfEvents, 
                            int64_t& numberOfBytesLeft)
{
    int64_t p = 0;
    int64_t s = 0;
    uint32_t bword = 0;
    const int64_t s_32 = bufferSize/4;
    const uint32_t *p_ui32 = (uint32_t*)&(buffer[0]);
    CITIROCEvent newEvent;
    static uint32_t datarow[97];
    while (p < s_32)
    {
        switch (s)
        {
        case 0:
            bword = p_ui32[p++];
            //Align to sync world
            if (((bword >> 4) & 0xc000000) == 0x8000000)
            {
                if(numberOfBytesLeft < eventSize_)
                {
                    p = s_32;
                    break;
                }
                s = 1;
                newEvent.AsicID = bword & 0xF;
                // newEvent.EventTimecode = (uint64_t) (p_ui32[p++]);
                p++;
                newEvent.RunEventTimecode = ((uint64_t)(p_ui32[p++]));
                newEvent.RunEventTimecode += ((uint64_t)(p_ui32[p++])) << 32L;
                newEvent.EventCounter = (uint64_t)(p_ui32[p++]);
            }
            else
                numberOfBytesLeft = bufferSize - p*4;

            break;

        case 1:
            for (int i = 0; i < 32; i++)
            {
                bword = p_ui32[p++];
                datarow[i * 3 + 0] = (bword >> 0) & 0x3FFF;
                datarow[i * 3 + 1] = (bword >> 14) & 0x3FFF;
                datarow[i * 3 + 2] = (bword >> 28) & 0x1;
            }
            int j;
            for (int i = 0; i < 32; i++)
            {
                j = 31-i;
                newEvent.hit += (datarow[j * 3 + 2] & 0x1) << i;
                int dataHG = (int)datarow[(j * 3) + 0];
                int dataLG = (int)datarow[(j * 3) + 1];

                newEvent.chargeHG[i] = (dataHG);
                newEvent.chargeLG[i] = (dataLG);
            }

            if (FW_USE_VALIDATION_ == 1)
                p += 3;
            s = 5;
            break;
                
        case 5:
            if ((p_ui32[p++] & 0xc0000000) == 0xc0000000)
            {
                events.push_back(newEvent);
                numberOfEvents++;
            }
            numberOfBytesLeft = bufferSize - p*4;
            s = 0;
            break;
        }
    }
}

void DT5550WBinFile::rewind() 
{
    fileObj_.seekg(0, std::ios_base::beg);
    numberOfEventsRead_ = 0;
}

bool DT5550WBinFile::isGood()
{
    return (fileObj_.peek() != EOF && (endPos_ - fileObj_.tellg()) > eventSize_);
}