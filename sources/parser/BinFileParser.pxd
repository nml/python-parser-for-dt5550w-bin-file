from libcpp.string cimport string as libcpp_string
from libcpp.vector cimport vector as libcpp_vector
from libcpp cimport bool
cdef extern from "BinFileParser.cpp":
    pass

cdef extern from "BinFileParser.h":
    cdef cppclass CITIROCEvent:
        CITIROCEvent() except +
        CITIROCEvent(CITIROCEvent&)
        unsigned long long RunEventTimecode # wrap-constant
        unsigned long long EventCounter # wrap-constant
        unsigned short AsicID # wrap-constant

        libcpp_vector[unsigned short] chargeLG # wrap-constant
        libcpp_vector[unsigned short] chargeHG # wrap-constant
        unsigned short hit # wrap-constant
    
    cdef cppclass DT5550WBinFile:  
        DT5550WBinFile(libcpp_string, bool) except +
        libcpp_vector[CITIROCEvent] readNextNEvents(int)
        libcpp_vector[CITIROCEvent] createNDummyEvents(int)
        int getNumberOfEventRead()
        void rewind()
        bool isGood()
