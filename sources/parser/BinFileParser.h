#pragma once
#include <fstream>
#include <vector>

class CITIROCEvent
{
public:
    CITIROCEvent(): chargeLG(32, 0), chargeHG(32, 0) {}
    // uint64_t EventTimecode;
    uint64_t RunEventTimecode;
    uint64_t EventCounter;
    uint16_t AsicID;

    // double EventTimecode_ns;
    double RunEventTimecode_ns;

    std::vector<uint16_t> chargeLG;
    std::vector<uint16_t> chargeHG;
    uint16_t   hit{0};

};

class DT5550WBinFile
{
private:
    std::ifstream fileObj_;
    int numberOfEventsRead_{0};
    bool FW_USE_VALIDATION_;
    int eventSize_{152};
    std::streampos endPos_;
public:
    
    DT5550WBinFile(std::string fpath, bool FW_USE_VALIDATION) :
        fileObj_(fpath, std::ios::binary), 
        FW_USE_VALIDATION_(FW_USE_VALIDATION)
    {
        if(!fileObj_.good())
        {
            throw std::ios_base::failure(fpath + " is not accessible.");
        }
        if (FW_USE_VALIDATION_)
        {
            eventSize_ += 12;
        }
        fileObj_.seekg(0, std::ios_base::end);
        endPos_ = fileObj_.tellg();
        fileObj_.seekg(0, std::ios_base::beg);
    }

    ~DT5550WBinFile()
    {
        this->fileObj_.close();
    }

    std::vector<CITIROCEvent> readNextNEvents(int n);
    std::vector<CITIROCEvent> createNDummyEvents(int n);
    void decode(const char* buffer, const int64_t bufferSize, std::vector<CITIROCEvent>& events, int& numberOfEvents, int64_t& numberOfBytesLeft);

    int getNumberOfEventRead() {return numberOfEventsRead_;}

    void rewind();

    bool isGood();

};