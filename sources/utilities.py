'''
Description: 
Author: Ming Fang
Date:  2022-09-19 15:43:38
LastEditors: Ming Fang
LastEditTime: 2022-09-20 19:44:45
'''
import json
import numpy as np
from typing import List, Tuple
from ast import literal_eval


class ImagerSetup:
    """Represents the setup of the imager, including mapping and calibration coefficients.
    """
    def __init__(self, fpath=None) -> None:
        if fpath is None:
            self._initializeSettings()
        else:
            self._loadSettingsFromJson(fpath)
        # Number of SiPMs
        self.NSiPMs = len(self._channelSettings)
        # Number of crystal
        self.NCrystals = self.NSiPMs // 2
        # Coincidence time window, ns
        self.timeWindow = 50
    
    def _loadSettingsFromJson(self, fpath) -> None:
        """Load the settings from a json file.

        Args:
            fpath (str | Path): Path to the json file.
        """
        with open(fpath) as f:
            s = json.load(f)
            self._channelSettings = {literal_eval(k): v for k, v in s.items()}
    
    def _initializeSettings(self) -> None:
        """Hard-coded settings. Should be updated if new acquisitions were taken.
        """
        # x coordinate of SiPM positions, cm
        xs = [-3.3, -2.2, -1.1, 0, 1.1, 2.2, 3.3]
        # y coordinate of SiPM positions, cm
        ys = [1.935, 0.645, -0.645, -1.935]
        # z coordinate of SiPM positions, cm
        zs = [-2.5, 2.5]
        # ASIC ID of all SiPM channels. Every two that are next to each other are on the same crystal.
        asicIDs = [0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3,
                   0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3,
                   2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1,
                   2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1]
        # Channel ID of all SiPM channels. Every two that are next to each other are on the same crystal.
        channelIDs = [15, 9, 14, 10, 13, 11, 12, 12, 11, 13, 10, 14, 9, 15, 
                       8, 2,  7,  3,  6,  4,  5,  5,  4,  6,  3,  7, 2,  8, 
                      15, 9, 14, 10, 13, 11, 12, 12, 11, 13, 10, 14, 9, 15, 
                       8, 2,  7,  3,  6,  4,  5,  5,  4,  6,  3,  7, 2,  8]
        # Energy calibration coefficients of all SiPM channels. Sorted by (ASICID, ChannelID).
        calibrationCoeffs = [[0.08262061, -157.93355263],
                             [1.00000000, 1.00000000000],
                             [0.07245192, -117.48250000],
                             [0.07416339, -66.369094490],
                             [0.07981992, -83.255720340],
                             [1.00000000, 1.00000000000],
                             [1.00000000, 1.00000000000],
                             [0.06321309, -56.266946310],
                             [1.00000000, 1.00000000000],
                             [1.00000000, 1.00000000000],
                             [0.06679965, -51.511170210],
                             [0.14490385, -388.45269231],
                             [0.06776079, -58.096942450],
                             [0.15191532, -225.16008065],
                             [0.09056490, -181.02283654],
                             [0.07720287, -117.23545082],
                             [0.09325495, -135.05173267],
                             [1.00000000, 1.00000000000],
                             [0.07475198, -157.42996032],
                             [0.06825181, -146.09365942],
                             [0.05203729, -46.524447510],
                             [0.07720287, -28.915368850],
                             [0.07595766, -43.773185480],
                             [1.00000000, 1.00000000000],
                             [0.09056490, -165.80793269],
                             [0.08562500, -166.11750000],
                             [0.08562500, -78.437500000],
                             [0.10350275, -225.16675824],
                             [1.00000000, 1.00000000000],
                             [0.15440574, -251.56844262],
                             [0.10350275, -165.54917582],
                             [0.10237772, -148.68722826],
                             [0.07595766, -159.83649194],
                             [0.08970238, -151.74880952],
                             [0.07358398, -103.88417969],
                             [0.08262061, -166.52609649],
                             [0.07657520, -155.24939024],
                             [0.06321309, -86.609228190],
                             [0.07189885, -92.202862600],
                             [0.08190217, -162.85630435],
                             [0.06540799, -147.67482639],
                             [0.08262061, -204.20109649],
                             [0.10952035, -169.87703488],
                             [0.08885613, -131.55731132],
                             [0.08190217, -131.40586957],
                             [0.06495690, -116.31327586],
                             [0.09234069, -120.88063725],
                             [0.08970238, -114.43261905],
                             [0.05999204, -5.8253184700],
                             [0.06495690, -96.046724140],
                             [0.10350275, -160.58104396],
                             [0.08641055, -117.44793578],
                             [0.08970238, -152.46642857],
                             [0.10582865, -185.17050562],
                             [0.08485360, -132.45067568],
                             [0.09710052, -144.94252577]]
        # Dictionary that saves settings of all SiPM channels.
        self._channelSettings = {}
        ii = 0
        # Energy threhsold, set to 100 keV. Events with E < 100 keV will be rejected.
        energyThreshold = 100  # keV
        for i in range(4):
            for j in range(7):
                for k in range(2):
                    asicID = asicIDs[ii]
                    channelID = channelIDs[ii]
                    calicoef = calibrationCoeffs[14*asicID+channelID-2]
                    enabled = True
                    if abs(calicoef[0] - 1) < 0.001 and abs(calicoef[1] - 1) < 0.001:
                        enabled = False
                    # currentChannel = Channel(enabled, xs[j], ys[i], zs[k], "LG", calicoef, energyThreshold)
                    currentChannel = {}
                    currentChannel['ID'] = ii
                    currentChannel['Enabled'] = enabled
                    if i == 0 or i == 3:
                        currentChannel['position'] = [xs[j], ys[i],zs[k]]
                    else:
                        currentChannel['position'] = [xs[6-j], ys[i],zs[k]]
                    currentChannel['Calibration Coeff'] = calicoef
                    currentChannel['Source'] = "LG"
                    currentChannel['Energy Threshold'] = energyThreshold
                    self._channelSettings[(asicID, channelID)] = currentChannel
                    ii+=1
    
    def isChannelEnabled(self, key: Tuple[int, int]) -> bool:
        """Check if a SiPM channel is enabled.

        Args:
            key (Tuple[int, int]): (AsicID, ChannelID) of the SiPM.

        Returns:
            bool: True is this SiPM channel is enabled.
        """
        if key not in self._channelSettings:
            return False
        return self._channelSettings[key]['Enabled']
    
    def saveSettings(self, fpath) -> None:
        """Save the settings to a json file.

        Args:
            fpath (str | Path): Path to the json file.
        """
        with open(fpath, 'w') as f:
            s = {str(k): v for k, v in self._channelSettings.items()}
            json.dump(s, f, indent=4)
    
    def getChannelID(self, key: Tuple[int, int]) -> int:
        """Get the unique ID of a SiPM channel.

        Args:
            key (Tuple[int, int]): (AsicID, ChannelID) of the SiPM.

        Returns:
            int: Unique ID assigned to this SiPM.
        """
        return self._channelSettings[key]['ID']
    
    def getChannelPosition(self, key: Tuple[int, int]) -> List:
        """Get the (x, y, z) coordinates of a SiPM.

        Args:
            key (Tuple[int, int]): (AsicID, ChannelID) of the SiPM.

        Returns:
            List: [x, y, z] coordinates, cm.
        """
        return self._channelSettings[key]['position']

    def getChannelCalibrationCoef(self, key: Tuple[int, int]) -> List:
        """Get the calibration coefficient of a SiPM channel.

        Args:
            key (Tuple[int, int]): (AsicID, ChannelID) of the SiPM.

        Returns:
            List: [k0, k2]. The calibrated energy corresponding to charge `x` is k0*x + k1.
        """
        return self._channelSettings[key]['Calibration Coeff']
    
    def getChannelSource(self, key: Tuple[int, int]) -> str:
        """Get the charge source used for energy calibration.

        Args:
            key (Tuple[int, int]): (AsicID, ChannelID) of the SiPM.

        Returns:
            str: Charge source, either "LG" or "HG".
        """
        return self._channelSettings[key]['Source']
    
    def getChannelEnergyThreshold(self, key: Tuple[int, int]) -> float:
        """Get the energy threshold of a SiPM channel.

        Args:
            key (Tuple[int, int]): (AsicID, ChannelID) of the SiPM.

        Returns:
            float: Energy threshold. Only events with deposited above this threshold will be processed.
        """
        return self._channelSettings[key]['Energy Threshold']
    
    def getAsicChannelID(self, id: int) -> Tuple[int, int]:
        """Get the AsicID and channel ID corresponding to the unique SiPM ID.

        Args:
            id (int): Unique ID assigned to a SiPM.

        Returns:
            Tuple[int, int]: (AsicID, ChannelID) of the SiPM.
        """
        if id < 0 or id >= self.NSiPMs:
            return None
        for k, v in self._channelSettings.items():
            if v['ID'] == id:
                return k


class Histogram:
    """A histogram object representing a histogram.
    """
    def __init__(self, xmin: float, xmax: float, nbins: int) -> None:
        """Initialize the Histogram object.

        Args:
            xmin (float): Left edge of the first bin.
            xmax (float): Right edge of the last bin.
            nbins (int): Number of bins between xmin and xmax.
        """
        self.binEdges, self.binWidth = np.linspace(xmin, xmax, num=(nbins+1), endpoint=True, retstep=True)
        # centers of the bins
        self.binCenters = (self.binEdges[:-1] + self.binEdges[1:]) / 2
        self.nBins = nbins
        # counts in each bin
        self.binCounts = np.zeros(self.binCenters.shape, dtype=int)
    
    def fill(self, x: float) -> None:
        """Find the bin that x falls into and increment the counts in that bin by 1.

        Args:
            x (float): A new occurence to be added to the histogram.
        """
        if x > self.binEdges[0] and x < self.binEdges[-1]:
            n = int((x-self.binEdges[0])/self.binWidth)
            self.binCounts[n] += 1
    
    def getTotalCounts(self) -> int:
        """Get the total number of counts so far.

        Returns:
            int: Total number of counts.
        """
        return np.sum(self.binCounts)


class SiPMEvent:
    """Represent an event from one SiPM channel.
    """
    def __init__(self) -> None:
        # unique id of SiPM, from 0 to SiPM_Number - 1
        self.SiPMID = None
        # ASIC ID, 0 or 1 or 2 or 3
        self.AsicID = None
        # Channel ID, 0-15
        self.channelID = None
        # time stamp, ns
        self.eventTimeStamp = None
        # light detected, keV
        self.lightDetected = None


class CrystalEvent:
    """Represent an energy deposition event in one crystal."""
    def __init__(self) -> None:
        # unique id of crystal, from 0 to Crystal_Number - 1
        self.crystalID = None
        # total energy deposited in this crystal during this event
        self.energyDeposited = None
        # time stamp of the event, ns
        self.eventTimeStamp = None
        # location of interaction, (x, y, z) in cm
        self.position = None
        # time difference between two SiPMs on top and bottom surfaces of the crystal
        self.deltaT = None


class CoincidenceEvent:
    """Represent a coincidence between two energy deposition events in two crystals.
    """
    def __init__(self) -> None:
        # Location of first interaction, (x, y, z) in cm
        self.firstInteractionPosition = None
        # Location of second interaction, (x, y, z) in cm
        self.secondInteractionPosition = None
        # Energy deposited in the first event, keV
        self.firstEnergyDeposited = None
        # Energy deposited in the second event, keV
        self.secondEnergyDeposited = None
