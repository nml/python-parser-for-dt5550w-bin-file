'''
Description: Test file for the paser.
Author: Ming Fang
Date: 2022-08-22 19:26:55
LastEditors: Ming Fang
LastEditTime: 2022-09-20 12:04:19
'''
import unittest
from sources.py_BinFileParser import CITIROCEvent, DT5550WBinFile


class TestBinParser(unittest.TestCase):
    def setUp(self):
        self.binFile = DT5550WBinFile(b'test_data/test.data', True)

    def testGetDummyEventsSuccess(self):
        chunkSize = 100
        newChunk = self.binFile.createNDummyEvents(chunkSize)
        assert len(newChunk) == chunkSize
        for i in range(len(newChunk)):
            self.assertEqual(newChunk[i].EventCounter, i)
            self.assertEqual(newChunk[i].AsicID, i)
            self.assertEqual(newChunk[i].RunEventTimecode, i)
            # self.assertAlmostEqual(newChunk[i].RunEventTimecode_ns, 0.5 * i)
            self.assertEqual(newChunk[i].hit, 0xFFFF)

    def testGetFirstEventSuccess(self):
        newEvent = (self.binFile.readNextNEvents(1))[0]
        self.assertEqual(self.binFile.getNumberOfEventRead(),
                         1)
        self.assertEqual(newEvent.AsicID, 1)
        self.assertEqual(newEvent.RunEventTimecode, 31439719)
        # self.assertAlmostEqual(newEvent.RunEventTimeCode_ns, 0.5 * 31439719)
        self.assertEqual(newEvent.EventCounter, 1)

        self.assertEqual(newEvent.hit, 0)
        
        chargeLG = [1486,  1526,  1470,  1508,  2733,  1550,  1429,  1546,  
                    1481,  1500,  1489,  1433,  1541,  1547,  1462,  1558,  
                    1469,  1572,  1495,  1522,  1532,  1547,  1507,  1447,  
                    1521,  1499,  1511,  1509,  1454,  1510,  1419,  1532]
        for i in range(len(chargeLG)):
            self.assertEqual(newEvent.chargeLG[i], chargeLG[i])
        
        chargeHG = [1577,  1663,  1494,  1525,  5474,  1554,  1338,  1379,  
                    1462,  1481,  1435,  1599,  1586,  1487,  1459,  1447,  
                    1556,  1529,  1603,  1573,  1540,  1472,  1525,  1462,  
                    1464,  1490,  1534,  1527,  1577,  1546,  1516,  1488]
        for i in range(len(chargeHG)):
            self.assertEqual(newEvent.chargeHG[i], chargeHG[i])

    def testGetSecondEventSuccess(self):
        self.binFile.readNextNEvents(1)
        newEvent = (self.binFile.readNextNEvents(1))[0]
        self.assertEqual(self.binFile.getNumberOfEventRead(), 2)
        self.assertEqual(newEvent.AsicID, 2)
        self.assertEqual(newEvent.RunEventTimecode, 31439721)
        self.assertEqual(newEvent.EventCounter, 2)

        self.assertEqual(newEvent.hit, 0)
        
        chargeLG = [1564,  1636,  1645,  1588,  1645,  1629,  3273,  1622,  
                    1549,  1576,  1484,  1583,  1589,  1528,  1561,  1501,  
                    1515,  1595,  1554,  1573,  1581,  1583,  1493,  1518,  
                    1611,  1572,  1579,  1611,  1579,  1599,  1590,  1531]
        for i in range(len(chargeLG)):
            self.assertEqual(newEvent.chargeLG[i], chargeLG[i])
        
        chargeHG = [1586,  1588,  1562,  1621,  1436,  1508,  7047,  1467, 
                    1559,  1480,  1441,  1359,  1452,  1339,  1439,  1457,  
                    1479,  1515,  1513,  1589,  1435,  1532,  1531,  1464,  
                    1541,  1551,  1512,  1471,  1462,  1501,  1568,  1563]
        for i in range(len(chargeHG)):
            self.assertEqual(newEvent.chargeHG[i], chargeHG[i])
    
    def testRewindSuccess(self):
        self.binFile.readNextNEvents(100)
        self.assertEqual(self.binFile.getNumberOfEventRead(), 100)
        self.binFile.rewind()
        self.assertEqual(self.binFile.getNumberOfEventRead(), 0)
        
        firstEvent = (self.binFile.readNextNEvents(1))[0]
        self.assertEqual(self.binFile.getNumberOfEventRead(), 1)
        self.assertEqual(firstEvent.AsicID, 1)
        self.assertEqual(firstEvent.RunEventTimecode, 31439719)
        self.assertEqual(firstEvent.EventCounter, 1)

    def testFileStatusSuccess(self):
        self.assertTrue(self.binFile.isGood())
        self.binFile.readNextNEvents(100)
        self.assertTrue(self.binFile.isGood())
        self.binFile.readNextNEvents(1000000)
        self.assertFalse(self.binFile.isGood())
        

if __name__ == '__main__':
    unittest.main()