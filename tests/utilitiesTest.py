'''
Description: Test file for utilities.py
Author: Ming Fang
Date: 2022-09-20 12:37:26
LastEditors: Ming Fang
LastEditTime: 2022-09-20 17:38:50
'''
import unittest
from sources.utilities import ImagerSetup, Histogram


class TestImagerSetup(unittest.TestCase):

    def testFirstChannelSuccess(self, imagerSetup: ImagerSetup=None):
        if imagerSetup is None:
            imagerSetup = ImagerSetup()
        # first channel
        key = (0, 15)
        self.assertEqual(imagerSetup.getChannelID(key), 0)
        self.assertTrue(imagerSetup.isChannelEnabled(key))
        posTrue = [-3.3, 1.935, -2.5]
        pos = imagerSetup.getChannelPosition(key)
        for i in range(3):
            self.assertAlmostEqual(pos[i], posTrue[i])
        caliCoefTrue = [0.15191532, -225.16008065]
        caliCoef = imagerSetup.getChannelCalibrationCoef(key)
        for i in range(2):
            self.assertAlmostEqual(caliCoef[i], caliCoefTrue[i])
        self.assertEqual(imagerSetup.getChannelSource(key), "LG")
        self.assertEqual(imagerSetup.getChannelEnergyThreshold(key), 100)
    
    def testDisabledChannelSuccess(self, imagerSetup: ImagerSetup=None):
        if imagerSetup is None:
            imagerSetup = ImagerSetup()
        # disabled channel
        key = (0, 3)
        self.assertEqual(imagerSetup.getChannelID(key), 24)
        self.assertFalse(imagerSetup.isChannelEnabled(key))
        posTrue = [-2.2, 0.645, -2.5]
        pos = imagerSetup.getChannelPosition(key)
        for i in range(3):
            self.assertAlmostEqual(pos[i], posTrue[i])
        caliCoefTrue = [1.0, 1.0]
        caliCoef = imagerSetup.getChannelCalibrationCoef(key)
        for i in range(2):
            self.assertAlmostEqual(caliCoef[i], caliCoefTrue[i])
        self.assertEqual(imagerSetup.getChannelSource(key), "LG")
        self.assertEqual(imagerSetup.getChannelEnergyThreshold(key), 100)
    
    def testLastChannelSuccess(self, imagerSetup: ImagerSetup=None):
        if imagerSetup is None:
            imagerSetup = ImagerSetup()
        # last channel
        key = (1, 8)
        self.assertEqual(imagerSetup.getChannelID(key), 55)
        self.assertTrue(imagerSetup.isChannelEnabled(key))
        posTrue = [3.3, -1.935, 2.5]
        pos = imagerSetup.getChannelPosition(key)
        for i in range(3):
            self.assertAlmostEqual(pos[i], posTrue[i])
        caliCoefTrue = [0.05203729, -46.524447510]
        caliCoef = imagerSetup.getChannelCalibrationCoef(key)
        for i in range(2):
            self.assertAlmostEqual(caliCoef[i], caliCoefTrue[i])
        self.assertEqual(imagerSetup.getChannelSource(key), "LG")
        self.assertEqual(imagerSetup.getChannelEnergyThreshold(key), 100)

    def testSaveSettingsSuccess(self):
        imagerSetup = ImagerSetup()
        imagerSetup.saveSettings('tests/testSettings.json')
    
    def testLoadSettingsSuccess(self):
        imagerSetup = ImagerSetup('tests/testSettings.json')
        self.testFirstChannelSuccess(imagerSetup)
        self.testDisabledChannelSuccess(imagerSetup)
        self.testLastChannelSuccess(imagerSetup)
        
    def testGetAsicChannelIDSuccess(self):
        imagerSetup = ImagerSetup()
        AsicID, channelID = imagerSetup.getAsicChannelID(0)
        self.assertEqual(AsicID, 0)
        self.assertEqual(channelID, 15)

class TestHistogram(unittest.TestCase):
    
    def testFillSuccess(self):
        xmin = 0
        xmax = 1
        nbins = 10
        histo = Histogram(xmin, xmax, nbins)
        binCentersExpected = [0.05, 0.15, 0.25, 0.35, 0.45,
                               0.55, 0.65, 0.75, 0.85, 0.95]
        for i in range(10):
            self.assertAlmostEqual(histo.binCenters[i], binCentersExpected[i])
        
        binCountsExpected = [0, 0, 0, 0, 0,
                             0, 0, 0, 0, 0]
        for i in range(10):
            self.assertEqual(histo.binCounts[i], binCountsExpected[i])
        
        for i in range(100):
            histo.fill(i/100+0.001)
        
        binCountsExpected = [10, 10, 10, 10, 10,
                             10, 10, 10, 10, 10]
        for i in range(10):
            self.assertEqual(histo.binCounts[i], binCountsExpected[i])


if __name__ == '__main__':
    unittest.main()