## Install Cython and autowrap
```bash
pip install Cython
```
```bash
pip install autowrap
```

## Compile the code
```bash
cd sources
python setupParser.py build_ext --inplace
cd ..
```

## Run tests
```bash
# python binFileParserTest.py
python -m unittest tests.binFileParserTest
python -m unittest tests.utilitiesTest
```

## Run example
```bash
python example.py
```

## Project Structure
- `example.py`: Main file. Execute this to process the data.
- `config.py`: Initialize gloable variables, including mapping (input) and spectra (output).
- `sources\utilities.py`: Custom data types. Apply new calibration coefficients here.
- `sources\setupParser.py`: Set up the binaray file parser.
- `sources\selectCoincidence.py`: Responsible for data processing, including energy calibration and coincidence selection.
- `sources\plotSpectra.py`: Plot the final results.