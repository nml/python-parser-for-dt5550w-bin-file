'''
Description: Initialize global variables
Author: Ming Fang
Date: 2022-09-20 14:31:28
LastEditors: Ming Fang
LastEditTime: 2022-09-20 17:39:31
'''
from sources.utilities import Histogram, ImagerSetup


# Imager setup
global imagerSetup
# Single SiPM spectra, calibrated
global singleSiPMSpectra
# Spectra of energy deposited in a single crystal (sum of light detected by two SiPMs)
global SiPMPairSpectra
# Spectra of total energy deposited in two crystals
global coincidenceSpectra
# list of coincidence events, needed for image reconstruction later
global coincidences

# use default settings
imagerSetup = ImagerSetup()
# # or load previous settings from file
# imagerSetup = ImagerSetup("tests/testSettings.json")
singleSiPMSpectra = [Histogram(0, 500, 200) for i in range(imagerSetup.NSiPMs)]
SiPMPairSpectra = [Histogram(0, 1000, 200) for i in range(imagerSetup.NCrystals)]
coincidenceSpectra = Histogram(0, 1000, 200)
coincidences = []
