'''
Description: 
Author: Ming Fang
Date: 2022-09-20 19:29:43
LastEditors: Ming Fang
LastEditTime: 2022-09-20 22:41:16
'''
import tkinter

# import the parser to use it
from sources.py_BinFileParser import CITIROCEvent, DT5550WBinFile
from sources.utilities import Histogram, ImagerSetup, SiPMEvent, CrystalEvent, CoincidenceEvent
from sources.selectCoincidence import processCITIROCEventChunk
from sources.plotSpectra import plotSpectra
import config


########### Data processing
# Read the file in chunks of 100k events until end of file (EOF)
# Depending on available memory size, chunkSize can be made smaller/larger
fileHandle = DT5550WBinFile(b'test_data/test.data', True)
chunkSize = 100000
# Max number of events to process
maxN = 1000000
numberOfPulsesRead = 0

while numberOfPulsesRead < maxN and fileHandle.isGood():
    newChunk = fileHandle.readNextNEvents(chunkSize)
    numberOfPulsesRead += len(newChunk)
    print("Read {} events.".format(len(newChunk)))
    processCITIROCEventChunk(newChunk)

########## Plot spectra
root = tkinter.Tk()
plotSpectra(root)
root.mainloop()
